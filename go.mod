module git.sr.ht/~taiite/senpai

go 1.16

require (
	git.sr.ht/~emersion/go-scfg v0.0.0-20201019143924-142a8aa629fc
	github.com/delthas/go-libnp v0.0.0-20221222161248-0e45ece1f878
	github.com/delthas/go-localeinfo v0.0.0-20221116001557-686a1e185118
	github.com/gdamore/tcell/v2 v2.5.4-0.20221017224006-ede1dd5ee680
	github.com/mattn/go-runewidth v0.0.14
	golang.org/x/net v0.0.0-20220722155237-a158d28d115b
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/time v0.0.0-20210611083556-38a9dc6acbc6
	mvdan.cc/xurls/v2 v2.3.0
)
