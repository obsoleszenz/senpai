package main

import (
	"flag"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"math/rand"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"
	"time"

	"git.sr.ht/~taiite/senpai"
	"github.com/gdamore/tcell/v2"
)

func main() {
	tcell.SetEncodingFallback(tcell.EncodingFallbackASCII)

	var configPath string
	var debug bool
	flag.StringVar(&configPath, "config", "", "path to the configuration file")
	flag.BoolVar(&debug, "debug", false, "show raw protocol data in the home buffer")
	flag.Parse()

	rand.Seed(time.Now().UnixNano())

	if configPath == "" {
		configDir, err := os.UserConfigDir()
		if err != nil {
			panic(err)
		}
		configPath = path.Join(configDir, "senpai", "senpai.scfg")
	}

	var configPathHash = configPathHash(configPath)

	cfg, err := senpai.LoadConfigFile(configPath)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to load the required configuration file at %q: %s\n", configPath, err)
		os.Exit(1)
	}

	cfg.Debug = cfg.Debug || debug
	buffers := getBuffers(configPathHash)
	cfg.Channels = append(cfg.Channels, buffers...)

	app, err := senpai.NewApp(cfg)
	if err != nil {
		panic(err)
	}

	lastNetID, lastBuffer := getLastBuffer(configPathHash)
	app.SwitchToBuffer(lastNetID, lastBuffer)
	app.SetLastClose(getLastStamp(configPathHash))

	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)

	go func() {
		<-sigCh
		app.Close()
	}()

	app.Run()

	app.Close()

	writeLastBuffer(configPathHash, app)
	writeLastStamp(configPathHash, app)
	writeBuffers(configPathHash, app)
}

func configPathHash(configPath string) string {
	h := fnv.New32a()
	h.Write([]byte(configPath))
	return fmt.Sprint(h.Sum32())
}

func cachePath() string {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		panic(err)
	}
	cache := path.Join(cacheDir, "senpai")
	err = os.MkdirAll(cache, 0755)
	if err != nil {
		panic(err)
	}
	return cache
}

func lastBufferPath(configPathHash string) string {
	var filename = fmt.Sprintf("lastbuffer-%s.txt", configPathHash)
	return path.Join(cachePath(), filename)
}

func getLastBuffer(configPathHash string) (netID, buffer string) {
	buf, err := ioutil.ReadFile(lastBufferPath(configPathHash))
	if err != nil {
		return "", ""
	}

	fields := strings.SplitN(strings.TrimSpace(string(buf)), " ", 2)
	if len(fields) < 2 {
		return "", ""
	}

	return fields[0], fields[1]
}

func writeLastBuffer(configPathHash string, app *senpai.App) {
	lastBufferPath := lastBufferPath(configPathHash)
	lastNetID, lastBuffer := app.CurrentBuffer()
	err := os.WriteFile(lastBufferPath, []byte(fmt.Sprintf("%s %s", lastNetID, lastBuffer)), 0666)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to write last buffer at %q: %s\n", lastBufferPath, err)
	}
}

func lastStampPath(configPathHash string) string {
	var filename = fmt.Sprintf("laststamp-%s.txt", configPathHash)
	return path.Join(cachePath(), filename)
}

func getLastStamp(configPathHash string) time.Time {
	buf, err := ioutil.ReadFile(lastStampPath(configPathHash))
	if err != nil {
		return time.Time{}
	}

	stamp := strings.TrimSpace(string(buf))
	t, err := time.Parse(time.RFC3339Nano, stamp)
	if err != nil {
		return time.Time{}
	}
	return t
}

func writeLastStamp(configPathHash string, app *senpai.App) {
	lastStampPath := lastStampPath(configPathHash)
	last := app.LastMessageTime()
	if last.IsZero() {
		return
	}
	err := os.WriteFile(lastStampPath, []byte(last.UTC().Format(time.RFC3339Nano)), 0666)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to write last stamp at %q: %s\n", lastStampPath, err)
	}
}

func buffersPath(configPathHash string) string {
	var filename = fmt.Sprintf("buffers-%s.txt", configPathHash)
	return path.Join(cachePath(), filename)
}

func getBuffers(configPathHash string) []string {
	buf, err := ioutil.ReadFile(buffersPath(configPathHash))
	if err != nil {
		return []string{}
	}

	buffers := strings.Split(strings.TrimSpace(string(buf)), " ")
	return buffers
}

func writeBuffers(configPathHash string, app *senpai.App) {
	buffersPath := buffersPath(configPathHash)
	buffers := app.Buffers()

	var buffersString = strings.Join(buffers, " ")

	err := os.WriteFile(buffersPath, []byte(buffersString), 0666)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to write buffers at %q: %s\n", buffersPath, err)
	}
}
